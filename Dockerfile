FROM alpine

# Update and install required packages
RUN apk update --no-cache
RUN apk add --no-cache \
curl \
mysql-client

# Make the directory that the database dump will be put in
RUN mkdir /db

COPY docker-cmd.sh /docker-cmd.sh
RUN chmod 744 /docker-cmd.sh

CMD ["sh", "docker-cmd.sh"]
