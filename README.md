# Mysql Database Init Container #

## What does this container do? ##

This container is used to initialize a mysql database from a sql dump downloaded from the internet.

To initialize the database the container will:

1. Check to see if the specified database has any tables in it. *If the container cannot connect to the database on the first try, it will try 20 times before failing*
    * **If the database is empty of tables**, it will proceed with the initialization
    * **If the database is not empty**, it will assume that the initialization has already been completed and will exit
2. Download the file from the specified URL using username and password if supplied
3. Run the SQL dump on the database

## How do I use it? ##

Run the container as follows:


```
#!shell
docker run -it \
-e DATABASE_NAME=my_db_name \
-e DATABASE_HOST=my_db_server \
-e DATABASE_PORT=my_db_port \
-e DATABASE_PASSWORD=my_db_pass \
-e DATABASE_DUMP_URL=url_to_db_dump \
-e DATABASE_DUMP_USER=user_to_use_when_downloading_dump \
-e DATABASE_DUMP_PASSWORD=password_to_use_when_downloading_dump \
katharostech/mysql-init
```