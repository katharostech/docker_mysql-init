# Check whether or not there are any tables in the database, retry up to 20 times
max_retries=20
attempt_number=0
echo "Checking database for existing tables"
while ! database_initialized=$(mysql -h${DATABASE_HOST} -P${DATABASE_PORT} -p${DATABASE_PASSWORD} -e"show tables;" ${DATABASE_NAME}) 
do
    echo "Couldn't connect, trying again."
    attempt_number=$(($attempt_number + 1))
    sleep 5

    if [ $attempt_number -eq $max_retries ];
    then
        echo "Couldn't connect to database! Giving up." > /dev/stderr
        exit 1
    fi
done

# If the database does not have any tables in it
if [ -z "${database_initialized}" ];
then 
    echo "The given database is empty proceeding with initialization"

    # Download the database dump from our bitbucket repo's downloads section
    echo "Downloading the sql dump from ${DATABASE_DUMP_URL}"
    curl -L -u ${DATABASE_DUMP_USER}:${DATABASE_DUMP_PASSWORD} ${DATABASE_DUMP_URL} > /db/database.sql
    curl_return_code=$?
     
    if [ ! ${curl_return_code} -eq 0 ];
    then
        echo "Downloading the sql dump failed!" > /dev/stderr
        exit 2
    fi

    # Load the database dump into the database
    echo "Trying to initialize database with sql dump"
    mysql -h${DATABASE_HOST} -P${DATABASE_PORT} -p${DATABASE_PASSWORD} ${DATABASE_NAME} < /db/database.sql
    echo "Database initialization complete"
    exit 0

# If the database already has tables init, don't do anything
else
    echo "The given database was not empty. Assuming that the database has already been initialized."
    echo "Database initialization complete"
    exit 0
fi
